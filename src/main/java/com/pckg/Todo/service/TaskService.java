package com.pckg.Todo.service;

import java.util.List;

import com.pckg.Todo.model.Task;

public interface TaskService {
    List<Task> getTasks();
    Task getTask(Long taskId);
    void createTask(Task task);
    void deleteTask(Long taskId);
    void updateTaskStatus(Long taskId, boolean status);
}
