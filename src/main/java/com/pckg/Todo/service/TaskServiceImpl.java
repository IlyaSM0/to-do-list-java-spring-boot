package com.pckg.Todo.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.pckg.Todo.model.Task;
import com.pckg.Todo.repository.TaskRepository;
import com.pckg.Todo.Tool.NotFoundException;

@Service
@Primary
public class TaskServiceImpl implements TaskService {

    private static final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public List<Task> getTasks() {
        List<Task> tasks = taskRepository.findAll();
        if (tasks.isEmpty()) {
            log.info("No tasks found");
        }
    
        log.info("Tasks list");
        return tasks;
    }

    @Override
    public Task getTask(Long taskId) {
        return taskRepository.findById(taskId)
            .orElseThrow(() -> new NotFoundException("Task not found with id "+ taskId));
    }
 
    @Override
    public void createTask(Task task) {
    task.setStatus(false);

    Task createdTask = taskRepository.save(task);

    if (createdTask == null || createdTask.getId() == null) {
        log.error("Failed to create task");
        throw new NotFoundException("Failed to create task");
    }

    log.info("Task created successfully");
}
    
    @Override
    public void deleteTask(Long taskId) {
    if (!taskRepository.existsById(taskId)) {
        throw new NotFoundException("Task not found with id " + taskId);
    }
    taskRepository.deleteById(taskId);
    log.info("Task " + taskId + " deleted successfully");
}
    
    
    @Override
    public void updateTaskStatus(Long taskId, boolean status) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new NotFoundException("Task not found with id: " + taskId));

        task.setStatus(status);
        taskRepository.save(task);
        log.info("Task" + taskId + " status updated sucessfully");
    }
}
