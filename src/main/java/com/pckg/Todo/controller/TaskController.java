package com.pckg.Todo.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pckg.Todo.model.Task;
import com.pckg.Todo.repository.TaskRepository;
import com.pckg.Todo.service.TaskServiceImpl;

@RestController
public class TaskController {

    private TaskServiceImpl Service;
    
    public TaskController(TaskServiceImpl service, TaskRepository repository) {
        this.Service = service;
  
    }

    @GetMapping("/")
    public ResponseEntity<String> app() {
        String message = "To-Do List, Welcome !";
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @GetMapping("/tasks")
    public ResponseEntity<List<Task>> getTasks() {
        List<Task> Tasks = Service.getTasks();
        return new ResponseEntity<>(Tasks, HttpStatus.OK);
    }

    @GetMapping("/find")
    public ResponseEntity<Task> getTask(@RequestParam Long id) {
        Task task = Service.getTask(id);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> addTask(@RequestBody Task task){
        Service.createTask(task);
        return new ResponseEntity<>("Task Added :)", HttpStatus.CREATED);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteTask(@RequestParam Long id) {
        Service.deleteTask(id);
    return new ResponseEntity<>("Task "+ id +" deleted :/", HttpStatus.OK);
    }

    @PutMapping("/mark")
    public ResponseEntity<String> updateTaskStatus(@RequestBody Task task) {
        Service.updateTaskStatus(task.getId(), task.isStatus());
        return new ResponseEntity<>("Task " + task.getId() +"status updated successfully ;)", HttpStatus.OK);
    }
}
