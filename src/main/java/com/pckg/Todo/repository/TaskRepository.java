package com.pckg.Todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pckg.Todo.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task,Long>{
    //Custom
}
