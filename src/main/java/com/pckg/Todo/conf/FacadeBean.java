package com.pckg.Todo.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pckg.Todo.service.TaskServiceImpl;


@Configuration
public class FacadeBean {
    @Bean
    public TaskServiceImpl TaskService() {
        return new TaskServiceImpl();
    }
}
