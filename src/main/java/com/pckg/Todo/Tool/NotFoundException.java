package com.pckg.Todo.Tool;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }
    
}

