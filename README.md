
# 📋 To-Do List (Java) 

Une application de gestion des tâches (CRUD), avec API REST & HTTP protocol

## Structure
La structure du projet est organisée de manière à faciliter la gestion des différentes composantes de l'application To-Do. Voici un aperçu des principaux répertoires et fichiers.

```diff
/Todo
 ├── /conf
 │   └── FacadeBean.java ----------> Fichier de configuration pour la gestion des façades.
 ├── /controller
 │   └── TaskController.java ------> Contrôleur gérant les requêtes et les réponses HTTP. 
 ├── /model
 │   └── Task.java ----------------> Modèle définissant la structure des données.
 ├── /repository
 │   └── TaskRepository.java ------> Gestion des interactions avec la base de données.
 ├── /service
 │   ├── TaskService.java ---------> Interface pour la logique métier.
 │   └── TaskServiceImpl.java -----> Implémentation de la logique métier.
 ├── /Tool
 │   └── NotFoundException.java ---> Classe utilitaire pour la gestion des exceptions.
 ├── TodoApplication.java ---------> Point d'entrée de l'application.
 ├── /resources
 │    └── application.properties --> Fichier de configuration de l'application.
 ├── To-Do-List.log ---------------> Fichier de logs.
 └── pom.xml ----------------------> Fichier de gestion des dépendances.
```

## 🚀 Installation 

#### Pré-requis :
- Un environnement de dev [Java-17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html).
- Téléharger et configurer [Maven-3.9.5](https://maven.apache.org/download.cgi).

#### Configuration :
- Ajouter ```mvn``` dans sa variable d'environnement **PATH**.

- Si votre **IDE** est VsCode, télécharger l'extension *Maven For Java* fournie par microsoft. 

- Configurer ```Maven › Executable: ``` dans les paramètres de l'extension et mettre : ```C:\path\apache-maven-3.9.5\bin\mvn```.

#### Clonez le projet :

```bash
git clone git@gitlab.com:IlyaSM0/to-do-list-java-spring-boot.git
```

#### Exécutez l'application :
```bash
mvn spring-boot:run
```
L'application prend par défault l'adresse : http://localhost:8080.


## Routes
Cet ensemble de routes reçoit et renvoie des données JSON.

| Opreator | Route     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `GET`      | `/`| Affiche un message de bienvenue. |
| `GET`      | `/tasks`| Retourne la liste des tâches. |
| `GET`      | `/find`| Retourne les détails d'une tâche spécifique. |
| `POST`      | `/new`| 	Ajoute une nouvelle tâche. |
| `DELETE`      | `/delete`| Supprime une tâche spécifique. |
| `PUT`      | `/mark`| Met à jour le statut d'une tâche. |


## ✍🏼 Authors
© 2023 [Ilyas SENOUSSA](ilyas.senoussa@supdevinci-edu.fr
).
